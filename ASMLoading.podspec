

Pod::Spec.new do |spec|

  spec.name         = "ASMLoading"
  spec.version      = "2.6.0"
  spec.summary      = "A short description of ASMLoading."

  spec.description  = "ASMLoading"
  spec.homepage     = "https://gitlab.com/Chumrussamee/asmloading"


  spec.license = { :type => "MIT", :file => "LICENSE" }
  spec.author             = { "OATZONE" => "oatzone1@hotmail.com" }


  spec.platform     = :ios, "11.0"
  spec.ios.deployment_target = "11.0"

  spec.source       = { :git => "https://gitlab.com/Chumrussamee/asmloading.git", :tag => "#{spec.version}" }

spec.source_files = 'ASMLoading/Classes/**/*.{swift}'
spec.resources = 'ASMLoading/Classes/**/*.{xcassets,png,jpeg,jpg,storyboard,xib,xcdatamodeld,json,ttf}'


#  spec.resources = "ASMLoading/**/*.{png,jpeg,jpg,storyboard,xib,xcassets,json,ttf}"



  spec.framework = "UIKit"
  spec.dependency 'lottie-ios', '~> 3.1'
  spec.swift_version = "5.0"
end
